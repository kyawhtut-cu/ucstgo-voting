var host = "ucstgo.mm.voting";
var photoDataOne = new FormData();
var photoDataTwo = new FormData();
var photoDataThree = new FormData();
var selection_id = "-1";
var selection_name = "";
var addSelectionModal;
var divPhotoOneDiv,divPhotoTwoDiv,divPhotoThreeDiv;
var firstPhotoUrl,secondPhotoUrl,thirdPhotoUrl;

$('#addSelectionDiv').load("resource/view/selection-add-view.html", function() {
	$addSelectionModal = $('#selectionAddDialog').modal({
		show: false
	});
	divPhotoOneDiv = $('#previewPhotoOne');
	divPhotoTwoDiv = $('#previewPhotoTwo');
	divPhotoThreeDiv = $('#previewPhotoThree');
	$('#delete-btn').on('click',function(){
		deleteSelection(selection_id,selection_name);
	})
	$('#selectionPhotoOne').change(function(e){
		$in=$(this);
		if($in.val() != ''){
			$('#removeFirstPhoto').css('display','block');
			$('#uploadPhotoOne').html("Upload");
			photoDataOne.append("file", e.target.files[0]);
			showPreviewImage(e.target.files,divPhotoOneDiv);
		}else{
			photoDataOne = new FormData();
			$('#uploadPhotoOne').html("Browse");
		}
	});
	$('#selectionPhotoTwo').change(function(e){
		$in=$(this);
		var fileName = e.target.files[0].name;
		if($in.val() != ''){
			$('#removeSecondPhoto').css('display','block');
			$('#uploadPhotoTwo').html("Upload");
			photoDataTwo.append("file", e.target.files[0]);
			showPreviewImage(e.target.files,divPhotoTwoDiv);
		}else{
			photoDataTwo = new FormData();
			$('#uploadPhotoTwo').html("Browse");
		}
	});
	$('#selectionPhotoThree').change(function(e){
		$in=$(this);
		var fileName = e.target.files[0].name;
		if($in.val() != ''){
			$('#removeThirdPhoto').css('display','block');
			$('#uploadPhotoThree').html("Upload");
			photoDataThree.append("file", e.target.files[0]);
			showPreviewImage(e.target.files,divPhotoThreeDiv);
		}else{
			photoDataThree = new FormData();
			$('#uploadPhotoThree').html("Browse");
		}
	});
	$('#btnCancel').on('click',function(){
		clearData();
	});
	$('#change-btn').on('click',function(){
		var photo = new Array();
		photo[0] = firstPhotoUrl;
		photo[1] = secondPhotoUrl;
		photo[2] = thirdPhotoUrl;
		photo = jQuery.grep(photo, function(n, i){
			return (n !== "" && n != null);
		});
		$.post(
			'insertSelectionData',
			{
				name : $('#name').val(),
				gender : $('input[name=gender]:checked').val(),
				selection_id : selection_id,
				class : $('input[name=class]:checked').val(),
				fbProfile : $('#fbProfile').val(),
				photo : photo
			},
			function success(data,status){
				console.log(data);
				getAllSelection();
			}
		);
	});
});

// getSelectionData(selection_id);

function getSelectionData(selection_id){
	$.post(
		'getSelectionData',
		{
			selection_id : selection_id
		},
		function success(data,status){
			if(selection_id!="-1"){
				bindData(data);
			}
		}
	);
}

function bindData(data){
	clearData();
	var oldData = new Array();
	$.each($.parseJSON(data), function (i,v)
	{
		oldData[i] = v;
	});
	$('#name').val(oldData[0]['name']);
	$('#fbProfile').val(oldData[0]['fbProfile']);
	if(oldData[0]['gender']==0){
		$('#male').prop("checked", true);
	}else{
		$("#female").prop("checked", true);
	}
	if(oldData[0]['class']=="Class - A"){
		$('#classA').prop("checked", true);
	}else{
		$('#classB').prop("checked", true);
	}
	firstPhotoUrl = oldData[0]['photo'][0];
	secondPhotoUrl = oldData[0]['photo'][1];
	thirdPhotoUrl = oldData[0]['photo'][2];
	$('#previewImgOne').remove();
	$('#previewImgThree').remove();
	$('#previewImgTwo').remove();
	if(firstPhotoUrl){
		var img = $('<img id="previewImgOne"/>').addClass('thumb').attr('src', firstPhotoUrl.replace("../","resource/"));
		divPhotoOneDiv.append(img);
	}
	if(secondPhotoUrl){
		var img = $('<img id="previewImgTwo"/>').addClass('thumb').attr('src', secondPhotoUrl.replace("../","resource/"));
		divPhotoTwoDiv.append(img);
	}
	if(thirdPhotoUrl){
		var img = $('<img id="previewImgThree"/>').addClass('thumb').attr('src', thirdPhotoUrl.replace("../","resource/"));
		divPhotoThreeDiv.append(img);
	}
	$('#change-btn').html("Update");
	$('#delete-btn').css('opacity',1);
	if(firstPhotoUrl){
		$('#removeFirstPhoto').css('display','block');
	}else{
		$('#removeFirstPhoto').css('display','none');
	}
	if(secondPhotoUrl){
		$('#removeSecondPhoto').css('display','block');
	}else{
		$('#removeSecondPhoto').css('display','none');
	}
	if(thirdPhotoUrl){
		$('#removeThirdPhoto').css('display','block');
	}else{
		$('#removeThirdPhoto').css('display','none');
	}
	selection_id = oldData[0]['selection_id'];
	selection_name = oldData[0]['name'];
	$addSelectionModal.modal('show');
}

function clearData(){
	selection_id = "-1";
	selection_name = "";
	firstPhotoUrl = "";
	secondPhotoUrl = "";
	thirdPhotoUrl = "";
	photoDataOne = new FormData();
	photoDataTwo = new FormData();
	photoDataThree = new FormData();
	$('#delete-btn').css('opacity',0);
	$('#change-btn').html("OK");
	$('#uploadPhotoOne').html("Browse");
	$('#uploadPhotoTwo').html("Browse");
	$('#uploadPhotoThree').html("Browse");
	$('#previewImgOne').remove();
	$('#previewImgTwo').remove();
	$('#previewImgThree').remove();
	$('#removeFirstPhoto').css('display','none');
	$('#removeSecondPhoto').css('display','none');
	$('#removeThirdPhoto').css('display','none');
	$('#name').val("");
	$('#fbProfile').val("");
	$('#selectionPhotoOne').val("");
	$('#selectionPhotoTwo').val("");
	$('#selectionPhotoThree').val("");
	$('#male').prop("checked", false);
	$('#female').prop("checked", false);
	$('#classA').prop("checked", false);
	$('#classB').prop("checked", false);
}

function uploadPhotoOne(){
	if($('#selectionPhotoOne').val()==''){
		$("#selectionPhotoOne").trigger("click");
	}else{
		deletePhoto(firstPhotoUrl);
		photoUpload(photoDataOne,divPhotoOneDiv);
	}
}

function uploadPhotoTwo(){
	if($('#selectionPhotoTwo').val()==''){
		$("#selectionPhotoTwo").trigger("click");
	}else{
		deletePhoto(secondPhotoUrl);
		photoUpload(photoDataTwo,divPhotoTwoDiv);
	}
}

function uploadPhotoThree(){
	if($('#selectionPhotoThree').val()==''){
		$("#selectionPhotoThree").trigger("click");
	}else{
		deletePhoto(thirdPhotoUrl);
		photoUpload(photoDataThree,divPhotoThreeDiv);
	}
}

function photoUpload(formData,select_div){
	$.ajax({
		url : "resource/class/photo-upload.php",
		method : "POST",
		data : formData,
		contentType : false,
		cache : false,
		processData : false,
		beforeSend : function(){
			showSnackBar(loading+"Uploading...");
		},   
		success : function(data) {
			console.log(data);
			if(data!="error"){
				showSnackBar("Upload success");
				if(select_div==divPhotoOneDiv){
					firstPhotoUrl = data;
					$('#selectionPhotoOne').val("");
					$('#uploadPhotoOne').html("Browse");
				}else if(select_div==divPhotoTwoDiv){
					secondPhotoUrl = data;
					$('#selectionPhotoTwo').val("");
					$('#uploadPhotoTwo').html("Browse");
				}else if(select_div==divPhotoThreeDiv){
					thirdPhotoUrl = data;
					$('#selectionPhotoThree').val("");
					$('#uploadPhotoThree').html("Browse");
				}
				showSweetAlert("Success","Upload success","success",1000);
			}else{
				showSnackBar("Upload error");
				if(select_div==divPhotoOneDiv){
					firstPhotoUrl = "";
				}else if(select_div==divPhotoTwoDiv){
					secondPhotoUrl = "";
				}else if(select_div==divPhotoThreeDiv){
					thirdPhotoUrl = "";
				}
				showSweetAlert("Error","Upload error","error",1000);
			}
			hideSnackBar(2000);
		}
	});
}

function showPreviewImage(files,select_div){
	$.each(files, function(index, file){
		if(/(\.|\/)(gif|jpe?g|png)$/i.test(file.type)){
			var fRead = new FileReader();
			fRead.onload = (function(file){
				return function(e) {
					if(select_div==divPhotoOneDiv){
						$('#previewImgOne').remove();
						var img = $('<img id="previewImgOne"/>').addClass('thumb').attr('src', e.target.result);
					}else if(select_div==divPhotoTwoDiv){
						$('#previewImgTwo').remove();
						var img = $('<img id="previewImgTwo"/>').addClass('thumb').attr('src', e.target.result);
					}else if(select_div==divPhotoThreeDiv){
						$('#previewImgThree').remove();
						var img = $('<img id="previewImgThree"/>').addClass('thumb').attr('src', e.target.result);
					}
					select_div.append(img);
				};
			})(file);
			fRead.readAsDataURL(file);
		}else{
			if(select_div==divPhotoOneDiv){
				removeFirstPhoto();
			}else if(select_div==divPhotoTwoDiv){
				removeSecondPhoto();
			}else if(select_div==divPhotoThreeDiv){
				removeThirdPhoto();
			}
			showSweetAlert("Error","Please select image type.(.jpg or .jpeg or .png or .gif)","error",3000);
		}
	});
}

function removeFirstPhoto(){
	$('#selectionPhotoOne').val("");
	$('#uploadPhotoOne').html("Browse");
	$('#previewImgOne').remove();
	$('#removeFirstPhoto').css('display','none');
	photoDataOne = new FormData();
	if(firstPhotoUrl!=""){
		deletePhoto(firstPhotoUrl);
		firstPhotoUrl = null;
	}
}

function removeSecondPhoto(){
	$('#selectionPhotoTwo').val("");
	$('#uploadPhotoTwo').html("Browse");
	$('#previewImgTwo').remove();
	$('#removeSecondPhoto').css('display','none');
	photoDataTwo = new FormData();
	if(secondPhotoUrl!=""){
		deletePhoto(secondPhotoUrl);
		secondPhotoUrl = null;
	}
}

function removeThirdPhoto(){
	$('#selectionPhotoThree').val("");
	$('#uploadPhotoThree').html("Browse");
	$('#previewImgThree').remove();
	$('#removeThirdPhoto').css('display','none');
	photoDataThree = new FormData();
	if(thirdPhotoUrl!=""){
		deletePhoto(thirdPhotoUrl);
		thirdPhotoUrl = null;
	}
}

function deleteSelection(selection_id,name){
	bootbox.confirm({ 
		backdrop: true,
		title: '<span class="glyphicon glyphicon-trash"></span> ဖျတ်မှာသေချာပါသလား။',
		size: "medium",
		message: name+" ၏ အချက်အလက်များကိုဖျတ်မှာသေချာပါသလား။", 
		callback: function(result){ 
			if(result){
				showSnackBar(loading+"Deleting "+name);
				$.post(
					'deleteSelection',
					{
						selection_id : selection_id
					},
					function success(data,status){
						console.log(data);
						selection_id = "-1";
						selection_name = "";
						clearData();
						showSnackBar("Delete success");
						getAllSelection();
						hideSnackBar(2000);
					}
				);
			}
		}
	});
}

function deletePhoto(path){
	showSnackBar(loading+" Deleting photo...")
	$.post(
		'resource/class/photo-upload.php',
		{
			path : path
		},
		function success(data,status){
			showSnackBar("Delete success");
			hideSnackBar(2000);
		}
	);
}