getDetailStudent(selectionId);

$('#nav-div').load("../resource/view/admin-nav.html",function(){
	votingYearId = $('#votingYear');
	$('#yearEdit').remove();
	$('#bs-example-navbar-collapse-1').remove();
	var headerHeight = ($('#nav-bar').height())+15;
	$('#votingYear').attr('href','home');
	$('#btn-nav').attr('data-target','#div-for-home');
	$('#main_div').css({
		"margin-top": headerHeight+"px",
		"padding-bottom": "15px"
	});
	$('#allResult').attr('href','../allresult');
	$('#selectionList').attr('href','../selectionlist');
	$('.navbar-brand').attr('href','../home');
	getUtilValue("voting_year");
});

$('#footer-div').load("../resource/view/admin-footer.html",function(){
	aboutId = $('#pAbout');
	$('#aboutEdit').remove();
	getUtilValue("about");
});

function  changeImage(view) {
	var href = view.src;
	$('#main_img').attr('src',href);
}

function getDetailStudent(selectionId){
	$.post(
		'../getDetailStudent',
		{
			selectionId : selectionId
		},
		function success(data,status){
			$('#main_div').html(data);
		}
	);
}

function getUtilValue(util_key){
	$.post(
		'../getUtilValue',
		{
			util_key : util_key
		},
		function success(data,status){
			if(util_key=="voting_year"){
				votingYearId.html(data);
			}else if(util_key=="voting_student_count"){
				voteStudentCount = data;
				stdCount.html("Student Count - "+data);
			}else if(util_key=="about"){
				about = data;
				aboutId.html(data);
			}
		}
	);
}