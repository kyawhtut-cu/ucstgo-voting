var voteStudentCount = 0;
var votingYear = "";
var serverStatusMsg = "";
var liveServerStatusMsg = "";
var votingYearId ;
var yearEditId;
var serverOpen = 0;
var liveServerOpen = 0;
var userNameId,stdCount;

$('#nav-div').load("resource/view/admin-nav.html",function(){
	votingYearId = $('#votingYear');
	userNameId = $('#user-name');
	stdCount = $('#stdCount');
	yearEditId = $('#yearEdit');
	$('#div-for-home').remove();
	getUserName();
	getUtilValue('server_open');
	getUtilValue('live_server_open');
	getUtilValue("voting_student_count");
	getUtilValue("voting_year");
	getUtilValue("server_close_msg");
	getUtilValue("live_server_close_msg");
	stdCount.on('click',function(){
		editUtilValue("voting_student_count","Enter Student Count","number","small",parseInt(voteStudentCount));
	});
	yearEditId.on('click',function(){
		editUtilValue("voting_year","Enter Voting Year","text","small",votingYear);
	});
	$('#serverStatusMsg').on('click',function(){
		editUtilValue("server_close_msg","Enter Server Status Message","textarea","large",serverStatusMsg);
	});
	$('#serverOpen').on('click',function(){
		if(serverOpen == 0){
			serverOpen = 1;
			$('#serverOpen').html("Vote Server already open.");
		}else{
			serverOpen = 0;
			$('#serverOpen').html("Vote Server already close.");
		}
		updateUtilValue("server_open",serverOpen);
	});
	$('#liveServerOpen').on('click', function() {
		if(liveServerOpen == 0) {
			liveServerOpen = 1;
			$('#liveServerOpen').html("Live Server already open");
		} else {
			liveServerOpen = 0;
			$('#liveServerOpen').html("Live Server already close.");
		}
		updateUtilValue('live_server_open', liveServerOpen);
	});
	$('#messageSend').on('click', function() {
		editUtilValue('messageSend', "Enter Notification Message", "textarea", 'large', "");
	});
	$('#liveServerStatusMsg').on('click', function() {
		editUtilValue('live_server_close_msg', "Enter Live Server Message", "textarea", 'large', liveServerStatusMsg);
	});
});

function getUserName(){
	$.post(
		'getUserName',
		function success(data,status){
			userNameId.html(data + ' <span class="caret"></span>');
		}
	);
}

function getUtilValue(util_key){
	$.post(
		'getUtilValue',
		{
			util_key : util_key
		},
		function success(data,status){
			if(util_key == "voting_year"){
				votingYear = data;
				votingYearId.html(data);
			} else if(util_key == "voting_student_count"){
				voteStudentCount = data;
				stdCount.html("Student Count - "+data);
			} else if(util_key == "about"){
				about = data;
				aboutId.html(data);
			} else if(util_key == "server_open"){
				serverOpen = data;
				if(serverOpen == 1){
					$('#serverOpen').html("Vote Server already open.");
				}
			} else if(util_key == "server_close_msg"){
				serverStatusMsg = data;
			} else if(util_key == "live_server_open") {
				liveServerOpen = data;
				if(liveServerOpen == 1) {
					$('#liveServerOpen').html("Live Server already open.");
				}
			} else if(util_key == "live_server_close_msg") {
				liveServerStatusMsg = data;
			}
		}
	);
}

function showSweetAlert(title,message,icon,time){
	if(message==1) message = ".";
	swal(message, {
		title: title,
		buttons: false,
		timer: time,
		icon: icon,
	});
}

function updateUtilValue(util_key,util_value){
	$.post(
		'updateUtilValue',
		{
			util_key : util_key,
			util_value : util_value
		},
		function(data){
			if(data){
				getUtilValue(util_key);
				showSweetAlert("Success",data,"success",1000);
			}else{
				showSweetAlert("Error","error","error",1000);
			}
		}
	).fail(function(error){
		showSweetAlert("Error",error,"error",1000);
	});
}

function sendMessage(message) {
	$.post(
		'sendMessage',
		{
			message: message
		},
		function(data) {
			if(data) {
				showSweetAlert("Success", "Successful send message", "success", 1000);
			} else {
				showSweetAlert("Error", "Error", "error", 1000);
			}
		}).fail(function(error) {
			showSweetAlert("Error", error, "error", 1000);
		});
}

bootbox.addLocale(
	"mm",{
		OK : 'JJ',
		CANCEL : 'DD',
		CONFIRM : 'DD'
	}
);
function editUtilValue(util_key,title,type,size,value){
	bootbox.prompt({
		value: value,
		size: size,

		title: "<font style='font-size: 20px; color: #2196f3;'>"+title+"</font>",
		inputType: type,
		callback: function(result){
			if(result){
				if(util_key == "messageSend") {
					sendMessage(result);
				} else {
					updateUtilValue(util_key, result);
				}
			}
		}
	});	
}