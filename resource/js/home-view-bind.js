var votingYear = "";
var aboutId;
var url = window.location.href;
var array = url.split('/');
var path = array[array.length-1];
if(!path){
	window.history.pushState("selectionlist", '', "selectionlist");
	path = "selectionlist";
}

$('#nav-div').load("resource/view/admin-nav.html",function(){
	votingYearId = $('#votingYear');
	$('#yearEdit').remove();
	$('#bs-example-navbar-collapse-1').remove();
	var headerHeight = $('#nav-bar').height();
	$('#votingYear').attr('href','home');
	$('#btn-nav').attr('data-target','#div-for-home');
	$('.base-div').attr('style', 'margin-top: '+headerHeight+'px;');
	getUtilValue("voting_year");
	$('#allResult').on('click',function(e){
		e.preventDefault();
		window.history.pushState($(this).attr('href'), '', $(this).attr('href'));
		path = $(this).attr('href');
		getAllSelectionForHome(path);
	});
	$('#selectionList').on('click',function(e){
		e.preventDefault();
		window.history.pushState($(this).attr('href'), '', $(this).attr('href'));
		path = $(this).attr('href');
		getAllSelectionForHome(path);
	});
});
$('#footer-div').load("resource/view/admin-footer.html",function(){
	aboutId = $('#pAbout');
	$('#aboutEdit').remove();
	getUtilValue("about");
	getAllSelectionForHome(path);
});
function getAllSelectionForHome(path){
	getToFiveResult();
	showSnackBar(loading + "Loading data resource...");
	$.post(
		'getAllSelectionList',
		{
			type: path
		},
		function success(data,status){
			$('#resultDiv').html(data);
			hideSnackBar(500);
			setInterval(function(){ 
				loadDataWithTime();
			}, 2000);
		}
	);
}
function getToFiveResult(){
	$.post(
		'getTopFiveResult',
		function success(data,status){
			$('.base-div').html(data);
		}
	);
}
function loadDataWithTime(){
	getToFiveResult();
	$.post(
		'getAllSelectionList',
		{
			type: path
		},
		function success(data,status){
			$('#resultDiv').html(data);
		}
	);
}
function getUtilValue(util_key){
	$.post(
		'getUtilValue',
		{
			util_key : util_key
		},
		function success(data,status){
			if(util_key=="voting_year"){
				votingYear = data;
				votingYearId.html(data);
			}else if(util_key=="about"){
				about = data;
				aboutId.html(data);
			}
		}
	);
}