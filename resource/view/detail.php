<?php
	$name = "";
	$app = new \Slim\Slim;
	$app->get('/detail/:selectionId',function($n){
		$selectionId = $n; ?>
		<script type="text/javascript">
			<?php
				echo "var selectionId = '{$selectionId}';";
			?>
		</script>
	<?php });
	$app->run();
?>
<!DOCTYPE html>
<html>
	<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

			<meta name='title' content='Fresher Welcome | UCSTgo'>
			<meta name='og:title' content='Fresher Welcome | UCSTgo'/>

			<meta name='description' content='၂၀၁၉ - ၂၀၂၀ ပညာသင်နှစ်၊ ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ) ၏ ပထမ နှစ် ကျောင်းသား/ကျောင်းသူ များ၏ Fresher Welcome တွင် King, Queen ရွေးချယ်ရန်။'>
			<meta property='og:description' content='၂၀၁၉ - ၂၀၂၀ ပညာသင်နှစ်၊ ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ) ၏ ပထမ နှစ် ကျောင်းသား/ကျောင်းသူ များ၏ Fresher Welcome တွင် King, Queen ရွေးချယ်ရန်။' />

			<meta name='og:keywords' content='ucstgo,cutgo,cu taungoo,ucs taungoo,ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ),cutgo voting, fresher welcome'/>
			<meta name='og:type' content='ucstgo,cutgo,cu taungoo,ucs taungoo,ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ),cutgo voting, fresher welcome'/>

			<meta property='og:image' content='https://ucstaungoo.app/voting/resource/img/ucst.jpg' />
			<meta name='og:image:alt' content='Fresher Welcome | UCSTgo'/>

			<meta name='og:author' content='Kyaw Htut'/>
			<meta name='author' content='Kyaw Htut'>

			<meta http-equiv="content-type" content="text/html; charset=UTF-8">

			<meta name="theme-color" content="#2196f3">
			<title>Fresher Welcome | UCSTgo</title>
			<link rel="shortcut icon" href="../resource/img/logo.png" />
			<link rel="stylesheet" href="../resource/css/bootstrap.min.css">
			<link rel="stylesheet" href="../resource/css/mystyle.css">
			<link rel="stylesheet" href="../resource/css/loading-style.css">
			<link rel="stylesheet" href="../resource/css/all.css">
			<link rel="stylesheet" href="../resource/css/lightbox.min.css">
			<link rel="stylesheet" href="../resource/css/Pretty-Footer.css">
	</head>
	<body style="background-color: #ccc!important;">
		<div id="nav-div"></div>
		<div id="main_div" class="container">
		</div>
		<div id="footer-div"></div>
		<script src="../resource/js/jquery.js"></script>
		<script src="../resource/js/popper.min.js"></script>
		<script src="../resource/js/bootstrap.min.js"></script>
		<script src="../resource/js/bootbox.min.js"></script>
		<script src="../resource/js/loadingoverlay.min.js"></script>
		<script src="../resource/js/lightbox.min.js"></script>
		<script src="../resource/js/detail-view.js"></script>
	</body>
</html>