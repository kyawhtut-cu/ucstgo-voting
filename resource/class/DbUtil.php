<?php
	class DbUtil {

		private $con = null;
		private $votingType = array(
		    "0_count_one" => "King မဲရလဒ် -",
		    "1_count_one" => "Queen မဲရလဒ် -",
		    "0_count_two" => "Prince မဲရလဒ် -",
		    "1_count_two" => "Princess မဲရလဒ် -",
		    "0_count_three" => "Inncocence မဲရလဒ် -",
		    "1_count_three" => "Inncocence မဲရလဒ် -"
		    );

		public static function Instance(){
			static $inst = null;
			if ($inst === null) {
				$inst = new DbUtil();
			}
			return $inst;
		}

		function __construct()
		{
			require 'resource/class/db-config.php';
			$this->con = $con;
		}

		function getCon(){
			return $this->con;
		}
		
		function getVotingType($gender, $voteType) {
		    return $this->votingType[$this->getGenderIndex($gender) . '_' . $voteType];
		}

		function isLoginUserAccount($user_name,$password){
			$user_name = mysqli_real_escape_string($this->getCon(),$user_name);
			$password = mysqli_real_escape_string($this->getCon(),$password);
			$query = "SELECT * FROM `admin` WHERE admin_user_name='$user_name' AND admin_password='$password'";
			$result = mysqli_query($this->getCon(),$query);
			if(mysqli_num_rows($result) == 1){
				return true;
			}else{
				return false;
			}
		}

		function getName($user_name,$password){
			$user_name = mysqli_real_escape_string($this->getCon(),$user_name);
			$query = "SELECT * FROM `admin` WHERE admin_user_name='$user_name' AND admin_password='$password'";
			$result = mysqli_query($this->getCon(),$query);
			if(mysqli_num_rows($result) == 1){
				$result = mysqli_fetch_assoc($result);
				echo $result['admin_display_name'];
			}else{
				echo 'Error';
			}
		}

		function getUtilValue($util_key){
			$query = "SELECT * FROM `voting_util` WHERE util_key='$util_key'";
			$result = mysqli_query($this->getCon(),$query);
			if(mysqli_num_rows($result) == 1){
				$result = mysqli_fetch_assoc($result);
				return $result['util_value'];
			}else{
				return "Error";
			}
		}

		function updateUtilValue($util_key,$util_value){
			$query = "UPDATE `voting_util` SET util_value='$util_value' WHERE util_key='$util_key'";
			$result = mysqli_query($this->getCon(),$query);
			if($util_key == "server_open" && $util_value == 1) {
				// send noti to all device about server open
				$this->sendServerOpenNotification();
			}
			return true;
		}

		function getSelectionCount($table){
			$query = "SELECT * FROM `$table`";
			$result = mysqli_query($this->getCon(),$query);
			return mysqli_num_rows($result) + 1;
		}

		function insertSelectonData($name,$gender,$selection_id,$class,$fbProfile,$photo){
			if($selection_id != "-1") {
				$query = "DELETE FROM `selection_photo` WHERE selection_id='$selection_id'";
				$result = mysqli_query($this->getCon(),$query);
				$query = "UPDATE `selection` SET name='$name', gender=$gender, class='$class', fbProfile='$fbProfile' WHERE selection_id='$selection_id'";
			} else {
				$selection_id ="selection-".$this->getSelectionCount('selection')."".md5(microtime().rand());
				$query = "INSERT INTO `selection`(name,gender,selection_id,class,fbProfile,count_one,count_two,count_three) VALUES('$name',$gender,'$selection_id','$class','$fbProfile',1,1,1)";
			}
			$result = mysqli_query($this->getCon(), $query);
			for($index = 0; $index < count($photo); $index++){
				$query = "INSERT INTO `selection_photo` (photo_url,selection_id) VALUES('$photo[$index]','$selection_id')";
				mysqli_query($this->getCon(),$query);
			}
			return "success";
		}
		
		function getGenderIndex($value) {
		    $gender = [
				"ယောင်္ကျားလေး",
				"မိန်းကလေး"
			];
			return array_search($value, $gender);
		}

		function getGenderEn($index){
			$gender = [
				"Male",
				"Female"
			];
			return $gender[$index];
		}

		function getGenderMm($index){
			$gender = [
				"ယောင်္ကျားလေး",
				"မိန်းကလေး"
			];
			return $gender[$index];
		}

		function checkApi($api){
			$query = "SELECT * FROM `api_key_user` WHERE api_key='$api'";
			$result = mysqli_query($this->getCon(),$query);
			if(mysqli_num_rows($result) == 1){
				return "success";
			}else{
				return "error";
			}
		}

		function getSelectionData($selection_id){
			$query = "SELECT * FROM `selection` WHERE selection_id='$selection_id'";
			$result = mysqli_query($this->getCon(),$query);
			$data = array();
			while($tmp = mysqli_fetch_assoc($result)){
				$selection_photo = $this->getSelectionPhoto($selection_id);
				$tmpArr['name'] = $tmp['name'];
				$tmpArr['gender'] = $tmp['gender'];
				$tmpArr['selection_id'] = $tmp['selection_id'];
				$tmpArr['class'] = $tmp['class'];
				$tmpArr['fbProfile'] = $tmp['fbProfile'];
				$tmpArr['photo'] = $selection_photo;
				array_push($data, $tmpArr);
			}
			return json_encode($data);
		}

		function getDetailStudent($selectionId){
			$result = json_decode($this->getDataWithQuery("SELECT * FROM `selection` WHERE selection_id = '" . $selectionId . "'", "Detail"));
			$data;$photo;
			foreach ($result as $key => $value) {
				$data = $value;
				$photo = $value->photo;
			}
			$path = str_replace('../', '../resource/', $photo[0]);
			$html = '
			<div class="card col-md-10">
				<div class="col-md-5" style="padding-top: 15px; padding-bottom: 15px; text-align: center;">
					<img src="'.$path.'" class="selectionPhoto-lg img-rounded" id="main_img"><br><br>
			';
			for($index = 0; $index<count($photo); $index++){
				$path = str_replace('../', '../resource/', $photo[$index]);
				$html .= '
					<img src="'.$path.'" class="selectionPhoto-sm img-rounded selected" onclick="changeImage(this);">
				';
			}
			$html .= '
				</div>
				<div class="col-md-7" style="padding-top: 15px; padding-bottom: 15px;">
					<h4 class="card-title"><a href="../detail/' . $data->name . '" style="text-decoration:none;">'.$data->name.'</a></h4>
					<a href="https://www.facebook.com/'.$data->fbProfile.'" target="_blank" style="text-decoration:none;">
						<small>
							<cite title="'.$data->name.' Facebook">
								ဖေ့စ်ဘွတ် အကောင့် <i class="glyphicon glyphicon-new-window"></i>
							</cite>
						</small>
					</a><br>
					<p>အခန်း - '.$data->class.'</p>
					<p>လိင်အမျိုးအစား	 - ' . $this->getGenderMm($data->gender) . '</p>
					<p>' . $this->getVotingType($this->getGenderMm($data->gender), "count_one") . ' ' . $this->getNumberWithMM($data->count_one) . '</p>
					<p>' . $this->getVotingType($this->getGenderMm($data->gender), "count_two") . ' ' . $this->getNumberWithMM($data->count_two).'</p>
					<p>' . $this->getVotingType($this->getGenderMm($data->gender), "count_three") . ' ' . $this->getNumberWithMM($data->count_three) . '</p>
					</div>
			</div>
			';
			echo $html;
		}

		function getTopFiveResult(){
			$query = "SELECT * FROM `selection` WHERE ";
			$king_data;$queen_data;$attractive_boy_data;$attractive_girl_data;
			$innocence_data;
			/*$innocence_boy_data;$innocence_girl_data;*/
			/*King*/
			$king = json_decode(
				$this->getDataWithQuery(
					$query . 
					'gender = 0 AND count_one <> 0 ORDER BY count_one DESC limit 1',
					"King"
				)
			);
			foreach ($king as $key => $value) {
				$king_data = $value;
				$attractive_boy = json_decode(
					$this->getDataWithQuery(
						$query . 
						'selection_id <> "' . $value->selection_id . '" AND ' . 
						'gender = 0 AND count_two <> 0 ORDER BY count_two DESC limit 1',
						"Prince"
					)
				);
				foreach ($attractive_boy as $key => $value) {
					$attractive_boy_data = $value;
				}
			}
			/*Queen*/
			$queen = json_decode(
				$this->getDataWithQuery(
					$query . 
					'gender = 1 AND count_one <> 0 ORDER BY count_one DESC limit 1',
					"Queen"
				)
			);
			foreach ($queen as $key => $value) {
				$queen_data = $value;
				$attractive_girl = json_decode(
					$this->getDataWithQuery(
						$query . 
						'selection_id <> "' . $value->selection_id . '" AND ' . 
						'gender = 1 AND count_two <> 0 ORDER BY count_two DESC  limit 1',
						"Princess"
					)
				);
				foreach ($attractive_girl as $key => $value) {
					$attractive_girl_data = $value;
				}
			}
			/*Innocence*/
			$innocence = json_decode(
				$this->getDataWithQuery(
					$query . 
					'selection_id <> "' . $king_data->selection_id . '" AND ' . 
					'selection_id <> "' . $queen_data->selection_id . '" AND ' . 
					'selection_id <> "' . $attractive_boy_data->selection_id .'" AND ' . 
					'selection_id <> "' . $attractive_girl_data->selection_id .'" AND ' . 
					'count_three <> 0 ORDER BY count_three DESC  limit 1',
					"Innocence"
				)
			);
			foreach ($innocence as $key => $value) {
				$innocence_data = $value;
			}
			/*Innocence Boy*/
			/*$innocenceBoy = json_decode(
				$this->getDataWithQuery(
					$query . 
					'selection_id <> "' . $king_data->selection_id . '" AND ' . 
					'selection_id <> "' . $attractive_boy_data->selection_id . '" AND ' . 
					'count_three <> 0 AND gender = 0 ORDER BY count_three DESC  limit 1',
					"Innocence Boy"
				)
			);
			foreach ($innocenceBoy as $key => $value) {
				$innocence_boy_data = $value;
			}*/
			/*Innocence Girl*/
			/*$innocenceGirl = json_decode(
				$this->getDataWithQuery(
					$query . 
					'selection_id <> "' . $queen_data->selection_id . '" AND ' . 
					'selection_id <> "' . $attractive_girl_data->selection_id . '" AND ' . 
					'count_three <> 0 AND gender = 1 ORDER BY count_three DESC  limit 1',
					"Innocence Girl"
				)
			);
			foreach ($innocenceGirl as $key => $value) {
				$innocence_girl_data = $value;
			}*/
			$html = '
				<div class="table-responsive">
					<table style="margin: 0 auto; float: none; border-color: transparent;">
						<tr>';
			$html .= $this->bindPhoto($king_data->photo, $king_data->name, "King");
			$html .= $this->bindPhoto($queen_data->photo, $queen_data->name, "Queen");
			$html .= $this->bindPhoto($attractive_boy_data->photo, $attractive_boy_data->name, "Prince");
			$html .= $this->bindPhoto($attractive_girl_data->photo, $attractive_girl_data->name, "Princess");
			$html .= $this->bindPhoto($innocence_data->photo, $innocence_data->name, "Innocence");
			/*$html .= $this->bindPhoto($innocence_boy_data->photo,$innocence_boy_data->name,"Innocence Boy");
			$html .= $this->bindPhoto($innocence_girl_data->photo,$innocence_girl_data->name,"Innocence Girl");*/
			$html .= '</tr>
					</table>
				</div>
			';
			echo $html;
		}

		function bindPhoto($photo,$name,$type){
			$html = '<td class="top5" title="'.$type.' '.$name.'">';
			for($index = 0 ; $index<count($photo); $index++){
				$path = str_replace('../', 'resource/', $photo[$index]);
				if($index==0){
					$html .= '
									<a style="float: left;" href="'.$path.'" data-lightbox="'.$type.' '.$name.'" data-title="'.$type.' '.$name.' ဓာတ်ပုံ">
										<img class="selectionPhoto rounded" src="'.$path.'" alt="'.$path.' ဓာတ်ပုံ">
									</a>';
				}else{
					$html .= '
									<a style="float: left;" href="'.$path.'" data-lightbox="'.$type.' '.$name.'" data-title="'.$type.' '.$name.' ဓာတ်ပုံ"></a>
					';
				}
			}
			$html .= '</td>';
			return $html;
		}

		function getDataWithQuery($query,$type){
			$result = mysqli_query($this->getCon(),$query);
			$data = array();
			while( $tmp = mysqli_fetch_assoc($result) ){
				$tmp['photo'] = $this->getSelectionPhoto($tmp['selection_id']);
				array_push($data, $tmp);
			}
			if(count($data)==0){
				$tmp['name'] = "Who $type?";
				$tmp['photo'] = [
					'../img/selection.img/whois.png'
				];
				$tmp['selection_id'] = "empty";
				array_push($data, $tmp);	
			}
			return json_encode($data);
		}

		function getSelectionPhoto($selection_id){
			$query = "SELECT * FROM `selection_photo` WHERE selection_id='$selection_id'";
			$result = mysqli_query($this->getCon(),$query);
			$data = array();
			while($tmp = mysqli_fetch_assoc($result)){
				$data[] = $tmp['photo_url'];
			}
			return $data;
		}

		function deleteSelection($selection_id){
			$query = "DELETE FROM `selection` WHERE selection_id='$selection_id'";
			mysqli_query($this->getCon(),$query);
			$query = "SELECT * FROM `selection_photo` WHERE selection_id='$selection_id'";
			$result = mysqli_query($this->getCon(),$query);
			while($tmp = mysqli_fetch_assoc($result)){
				$path = $tmp['photo_url'];
				$path = str_replace("../","resource/",$path);
				if( file_exists($path) ){
					unlink($path);
				}
			}
			$query = "DELETE FROM `selection_photo` WHERE selection_id='$selection_id'";
			mysqli_query($this->getCon(),$query);
			echo "success";
		}

		function getAllSelectionDataForAdmin(){
			$query = "SELECT * FROM `selection`";
			$result = mysqli_query($this->getCon(),$query);
			$data = array();
			while($tmp = mysqli_fetch_assoc($result)){
				$selection_id = $tmp['selection_id'];
				$tmp['gender'] = $this->getGenderMm($tmp['gender']);
				$tmp['photo'] = $this->getSelectionPhoto($selection_id);
				array_push($data, $tmp);
			}
			return json_encode($data);
		}

		function getAllSelectionList($type){
			$data = json_decode($this->getAllSelectionDataForAdmin());
			$html = "";
			foreach ($data as $key => $value) {
				if($type === "selectionlist"){
					$html .= $this->getSelectionListData($value);
				}else if($type === "allresult"){
					$html .= $this->getSelectionDataWithResult($value);
				}
			}
			return $html;
		}

		function getNumberWithMM($value){
			$num = ['၀','၁','၂','၃','၄','၅','၆','၇','၈','၉'];
			$num_value = "";
			for($index = 0; $index < strlen($value); $index++){
				$num_value .= $num[$value[$index]];
			}
			if($num_value==$num[0]){
				$num_value = "မဲပေးသူမရှိပါ။";
			}
			return $num_value;
		}

		function getSelectionDataWithResult($value){
			$name = $value->name;
			$selection_id = $value->selection_id;
			$url = 'detail/'. $selection_id;
			$gender = $value->gender;
			$class = $value->class;
			$fbProfile = $value->fbProfile;
			$photo = $value->photo;
			$count_one = $value->count_one;
			$count_two = $value->count_two;
			$count_three = $value->count_three;
			$html = '<div class="col-xs-12 col-sm-6 col-md-4">
								<div class=" well well-sm">';
			for($index = 0 ; $index<count($photo); $index++){
				$path = str_replace('../', 'resource/', $photo[$index]);
				if($index==0){
					$html .= '
									<a style="float: left;" href="'.$path.'" data-lightbox="'.$name.'" data-title="'.$name.' ဓာတ်ပုံ">
										<img src="'.$path.'" alt="'.$path.' ဓာတ်ပုံ" class="selectionPhoto img-rounded" />
									</a>';
				}else{
					$html .= '
									<a style="float: left;" href="'.$path.'" data-lightbox="'.$name.'" data-title="'.$name.' ဓာတ်ပုံ"></a>
					';
				}
			}					
			$html .= '			<div style="float: left; margin-left: 10px;">
										<h4 class="card-title"><a charset="UTF-8" href="'.$url.'" style="text-decoration:none;">'.$name.'</a></h4>
										<a href="https://www.facebook.com/'.$fbProfile.'" target="_blank" style="text-decoration:none;">
											<small>
												<cite title="'.$fbProfile.' Facebook">
													ဖေ့စ်ဘွတ် အကောင့် <i class="glyphicon glyphicon-new-window"></i>
												</cite>
											</small>
										</a>
										<table class="table table-condensed table-responsive table-user-information" style="margin-bottom: 0px;">
											<tbody>
												<tr>
													<td>အခန်း</td>
													<td align="right">'.$class.'</td>
												</tr>
												<tr>
													<td>လိင်အမျိုးအစား</td>
													<td align="right">'.$gender.'</td>
												</tr>
												<tr>
													<td>' . $this->getVotingType($gender, "count_one") . '</td>
													<td align="right">'.$this->getNumberWithMM($count_one).'</td>
												</tr>
												<tr>
													<td>' . $this->getVotingType($gender, "count_two") . '</td>
													<td align="right">'.$this->getNumberWithMM($count_two).'</td>
												</tr>
												<tr>
													<td>' . $this->getVotingType($gender, "count_three") . '</td>
													<td align="right">'.$this->getNumberWithMM($count_three).'</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div style="clear: both;"></div>
								</div>
							</div>';
			return $html;
		}

		function getSelectionListData($value){
			$name = $value->name;
			$gender = $value->gender;
			$selection_id = $value->selection_id;
			$class = $value->class;
			$fbProfile = $value->fbProfile;
			$photo = $value->photo;
			$url = 'detail/' . $selection_id;
			$html = '<div class="col-xs-12 col-sm-6 col-md-4">
								<div class="well" style="padding: 10px;">
									<div class="card-body">
										<p>';
			for($index = 0; $index<count($photo); $index++){
				$path = str_replace('../', 'resource/', $photo[$index]);
				if($index==0){
							$html .= '
											<a style="float: left; margin-right: 5px;" href="'.$path.'" data-lightbox="'.$name.'" data-title="'.$name.' ဓာတ်ပုံ">
												<img class=" img-fluid" src="'.$path.'" alt="'.$name.' ဓာတ်ပုံ">
											</a>';
				}else{
							$html .= '
											<a style="float: left;" href="'.$path.'" data-lightbox="'.$name.'" data-title="'.$name.' ဓာတ်ပုံ"></a>';
				}
			}
			$html .= '
										</p>
										<h4 class="card-title"><a charset="UTF-8" href="'.$url.'" style="text-decoration:none;">'.$name.'</a></h4>
										<a href="https://www.facebook.com/'.$fbProfile.'" target="_blank" style="text-decoration: none;">
											<small>
												<cite title="'.$name.' Facebook">
													ဖေ့စ်ဘွတ် အကောင့် <i class="glyphicon glyphicon-new-window"></i>
												</cite>
											</small>
										</a>
										<p style="margin-top: 5px;">'.$class.'</p>
										<a onclick="voteNow(\''.$name.'\');" class="btn btn-web-primary btn-sm"><i class="glyphicon glyphicon-heart"> Vote now.</i></a>
									</div>
									<div style="clear: both;"></div>
								</div>
							</div>';
			return $html;
		}
		
		function getSelectionDetailForV2($api_key, $selectionId) {
		    if($this->checkApi($api_key) == "success") {
			        if($this->getUtilValue("live_server_open") == 1) {
			        	$data = json_decode($this->getDataWithQuery("SELECT * FROM `selection` WHERE selection_id = '" . $selectionId . "'","Detail"))[0];
			        	if($data->selection_id != "empty") {
			            	echo '{"selection": ' . json_encode($data) . '}';
			            } else {
			            	echo '{"message" : "ရွေးချယ်ခံရသူကို ရှာဖွေလို့မတွေ့ပါ။"}';
			            }
			        } else {
			            echo '{"message" : "' . $this->getUtilValue("live_server_close_msg") . '"}';
			        }
		    } else {
		        echo '{"message" : "Api key error."}';
		    }
		}

		function getAllSelectionListForV2($api_key){
			if($this->checkApi($api_key) == "success"){
				if($this->getUtilValue('server_open') == 1){
					echo '{"selection":'.$this->getDataWithQuery('SELECT * FROM `selection`','type').'}';
				}else{
					echo '{"message":"'.$this->getUtilValue("server_close_msg").'"}';
				}
			}else{
				echo '{"message":"Api key error"}';
			}
		}

		function updateQuery($selection_id,$column){
			$query = "UPDATE `selection` SET ".$column." = ".$column."+1 WHERE selection_id='".$selection_id."'";
			mysqli_query($this->getCon(),$query);
		}

		function updateVotingCountForV2($api_key,$king_id,$queen_id,$att_boy_id,$att_girl_id,$innocence_boy_id, $innocence_girl_id, $token){
			if($this->checkApi($api_key)=="success"){
				if($this->getUtilValue('server_open') == 1){
					$this->updateQuery($king_id,"count_one");
					$this->updateQuery($queen_id,"count_one");
					$this->updateQuery($att_boy_id,"count_two");
					$this->updateQuery($att_girl_id,"count_two");
					$this->updateQuery($innocence_boy_id,"count_three");
					/*$this->updateQuery($innocence_girl_id,"count_three");*/
					echo '{"message":"Voting success"}';
					$this->sendFirebaseMessage($token);
				}else{
					echo '{"message":"'.$this->getUtilValue("server_close_msg").'"}';
				}
			}else{
				echo '{"message":"Api key error"}';
			}
		}
		
		function loginForV2($api_key, $email, $password) {
		    if($this->checkApi($api_key) == "success") {
		        $email = mysqli_real_escape_string($this->getCon(), $email);
			    $password = mysqli_real_escape_string($this->getCon(), $password);
			    $password = md5($password);
			    $query = "SELECT * FROM `admin` WHERE admin_user_name='$email' AND admin_password='$password'";
			    $result = mysqli_query($this->getCon(), $query);
			    if(mysqli_num_rows($result) == 1){
				    echo '{"message": "' . $api_key . '"}';
			    }else{
				    echo '{"message": "User name or password incorrect"}';
			    }
		    } else {
		        echo '{"message":"Api key error"}';
		    }
		}

		function getCURL() {
			$handle = curl_init();
			curl_setopt(
				$handle,
				CURLOPT_URL,
				"https://fcm.googleapis.com/fcm/send"
			);
			curl_setopt(
				$handle,
				CURLOPT_POST,
				1
			);
			curl_setopt(
				$handle,
				CURLOPT_RETURNTRANSFER,
				true
			);
			$headers = [
				'Authorization:key=AAAAE9UPwhA:APA91bFvZgAAA2kF86BASLGwbK_lKkYKbL03PIW46Z3A1WY86Q40RQ8qzrg8nFJAUek7Vekk2JWkuLcU0D9zx-ih5x3jC1Y2oQBCi8H6CVrsBa81F5hiGhS7CsHCcXLVZYkJPRotk1Gur4T-rvjiPATtwPkj7hdEwg',
				'Content-Type:application/json'
			];
			curl_setopt(
				$handle,
				CURLOPT_HTTPHEADER,
				$headers
			);
			return $handle;
		}

		function sendMessage($message) {
			$vars = [
				"to" => "/topics/all",
				"notification" => [
					"body" => $message,
					"title" => "Admin Send"
				],
				"data" => [
					"type" => "/",
					"message" => $message
				]
			];
			$handle = $this->getCURL();
			curl_setopt(
				$handle,
				CURLOPT_POSTFIELDS,
				json_encode($vars)
			);  //Post Fields
			$data = json_decode(curl_exec($handle));
			$responseCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
			curl_close ($handle);
		}

		function sendServerOpenNotification() {
			$vars = [
				"to" => "/topics/all",
				"notification" => [
					"body" => "Voting ပေးလို့ရပါပြီ ခဗျာ။ Vote ပေးပြီးပါက မုန့်ထုတ်ယူလို့ရပါပြီ။",
					"title" => "Voting server ဖွင့်လိုက်ပါပြီ။"
				],
				"data" => [
					"type" => "/",
					"message" => "Voting ပေးလို့ရပါပြီ ခဗျာ။ Vote ပေးပြီးပါက မုန့်ထုတ်ယူလို့ရပါပြီ။"
				]
			];
			$handle = $this->getCURL();
			curl_setopt(
				$handle,
				CURLOPT_POSTFIELDS,
				json_encode($vars)
			);  //Post Fields
			$data = json_decode(curl_exec($handle));
			$responseCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
			curl_close ($handle);
		}

		function sendFirebaseMessage($token) {
			$vars = [
				"to" => $token,
				"notification" => [
					"body" => "မဲပေးမှု အောင်မြင်ပါသည်။​ ယခုပဲ မုန့်ယူလို့ရပါပြီ။",
					"title" => "အောင်မြင်ပါသည်။​"
				],
				"data" => [
					"type" => "voting",
					"message" => "မဲပေးမှု အောင်မြင်ပါသည်။​ ယခုပဲ မုန့်ယူလို့ရပါပြီ။"
				]
			];
			$handle = $this->getCURL();
			curl_setopt(
				$handle,
				CURLOPT_POSTFIELDS,
				json_encode($vars)
			);  //Post Fields
			$data = json_decode(curl_exec($handle));
			$responseCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
		}
	}