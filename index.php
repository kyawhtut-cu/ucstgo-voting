<?php 
	
	require 'resource/class/DbUtil.php';
	require 'Slim/Slim.php';

	use SimpleExcel\SimpleExcel;
	session_start();

	\Slim\Slim::registerAutoloader();

	$app = new \Slim\Slim;

	$app->get('/password/:password',function($password){
		echo md5($password);
	});

	$app->get('/',function() use($app){
		require 'resource/view/home.html';
	})->name('home');

	$app->get('/allresult',function(){
		require 'resource/view/home.html';
	});
	$app->get('/selectionlist',function(){
		require 'resource/view/home.html';
	});

	$app->get('/home',function() use($app){
		$app->redirect($app->urlFor('home'));
	});

	$app->get('/detail/:selectionId',function(){
		require 'resource/view/detail.php';
	});

	$app->get('/admin-ucstgo/login',function() use($app){
		if(isLogin()){
			$app->redirect($app->urlFor('cutgo-view'));
		}else{
			require 'resource/view/login-view.html';
		}
	})->name('login');

	$app->get('/logout',function() use($app){
		session_unset();
		session_destroy();
		$app->redirect($app->urlFor('login'));
	});

	$app->get('/admin-ucstgo',function() use($app){
		if(isLogin()){
			require 'resource/view/admin-view.html';
		}else{
			$app->redirect($app->urlFor('login'));
		}
	})->name('cutgo-view');

	$app->post('/login-check',function() use($app){
		$url = require 'resource/class/login-check.php';
		if($url==="success"){
			$app->redirect($app->urlFor('cutgo-view'));
		}else{
			return $app->redirect($url);
		}
	});

	/*$app->get('/not-found',function(){
		require 'resource/view/page-not-found.html';
	})->name('not-found');*/

	$app->get('/change-password',function() use($app){
		$old = $app->request->get('old_password');
		$new = $app->request->get('new_password');
		$dbutil = DbUtil::Instance();
		$dbutil->changePassword($dbutil->getCon(),$old,$new,$_SESSION['user_name']);
	});

	$app->post('/getUtilValue/',function() use($app){
		$util_key = $app->request->post('util_key');
		$dbutil = DbUtil::Instance();
		echo $dbutil->getUtilValue($util_key);
	});

	$app->post('/updateUtilValue/',function() use($app){
		$util_key = $app->request->post('util_key');
		$util_value = $app->request->post('util_value');
		$dbutil = DbUtil::Instance();
		echo $dbutil->updateUtilValue($util_key,$util_value);
	});

	$app->post('/sendMessage', function() use($app) {
		if(!isLogin()){
			echo '{"message": "Unauthorized Request."}';
		} else if($app->request->post('message') != null){
			$message = $app->request->post('message');
			echo DbUtil::Instance()->sendMessage($message);
		} else {
			echo '{"message":"Bad Request."}';
		}
	});

	$app->post('/getUserName/',function() use($app){
		$dbutil = DbUtil::Instance();
		echo $dbutil->getName($_SESSION['user_name'],$_SESSION['password']);
	});

	$app->post('/getSelectionData/',function() use($app){
		$selection_id = $app->request->post('selection_id');
		$dbutil = DbUtil::Instance();
		echo $dbutil->getSelectionData($selection_id);
	});

	$app->post('/getAllSelectionList/',function() use($app){
		$type = $app->request->post('type');
		$dbutil = DbUtil::Instance();
		$print_html = $dbutil->getUtilValue('live_server_open');
		if($print_html == 1){
			$print_html = $dbutil->getAllSelectionList($type);
		}else{
			$print_html = '<h4 class="card-title" style="padding-left: 15px; padding-right: 15px; line-height: 1.6;" align="center">'.$dbutil->getUtilValue('live_server_close_msg').'</h4>';
		}
		echo $print_html;
	});

	$app->post('/getTopFiveResult/',function(){
		$dbutil = DbUtil::Instance();
		$print_html = $dbutil->getUtilValue('live_server_open');
		if($print_html == 1){
			$print_html = $dbutil->getTopFiveResult();
		}else{
			$print_html = '
				<div class="table-responsive">
					<table style="margin: 0 auto; float: none; border-color: transparent;">
						<tr>
							<td>
								<img class="selectionPhoto rounded" src="resource/img/selection.img/whois.png">
							</td>
							<td class="top5">
								<img class="selectionPhoto rounded" src="resource/img/selection.img/whois.png">
							</td>
							<td>
								<img class="selectionPhoto rounded" src="resource/img/selection.img/whois.png">
							</td>
							<td class="top5">
								<img class="selectionPhoto rounded" src="resource/img/selection.img/whois.png">
							</td>
							<td>
								<img class="selectionPhoto rounded" src="resource/img/selection.img/whois.png">
							</td>
						</tr>
					</table>
				</div>
			';
		}
		echo $print_html;
	});

	$app->post('/insertSelectionData/',function() use($app){
		$name = $app->request->post('name');
		$gender = $app->request->post('gender');
		$selection_id = $app->request->post('selection_id');
		$class = $app->request->post('class');
		$fbProfile = $app->request->post('fbProfile');
		$photo = $app->request->post('photo');
		$dbutil = DbUtil::Instance();
		echo $dbutil->insertSelectonData($name,$gender,$selection_id,$class,$fbProfile,$photo);
	});

	$app->post('/getAllSelection/',function(){
		$dbutil = DbUtil::Instance();
		echo $dbutil->getAllSelectionDataForAdmin();
	});

	$app->post('/deleteSelection/',function() use($app){
		$selection_id = $app->request->post('selection_id');
		$dbutil = DbUtil::Instance();
		$dbutil->deleteSelection($selection_id);
	});

	$app->post('/getDetailStudent/',function() use($app){
		$name = $app->request->post('selectionId');
		$dbutil = DbUtil::Instance();
		$dbutil->getDetailStudent($name);
	});

	$app->post('/v2.api/getAllSelectionList/:api_key',function($api_key){
		$dbUtil = DbUtil::Instance();
		$dbUtil->getAllSelectionListForV2($api_key);
	});

	$app->post('/v2.api/updateVotingUser/:api_key',function($api_key) use($app){
		if($app->request->post('king_id') !=null &&
			$app->request->post('queen_id') !=null &&
			$app->request->post('att_boy_id') !=null &&
			$app->request->post('att_girl_id') != null &&
			$app->request->post('innocence_id') != null &&
			$app->request->post('token') != null
			/*$app->request->post('innocence_boy_id') != null &&
			$app->request->post('innocence_girl_id') != null*/
		){
			$dbutil = DbUtil::Instance();
			$dbutil->updateVotingCountForV2(
				$api_key,
				$app->request->post('king_id'),
				$app->request->post('queen_id'),
				$app->request->post('att_boy_id'),
				$app->request->post('att_girl_id'),
				$app->request->post('innocence_id'),
				"",
				$app->request->post('token')
				/*$app->request->post('innocence_boy_id'),
				$app->request->post('innocence_girl_id')*/
			);
		} else {
			echo '{"message":"Request error."}';
		}
	});
	
	$app->post("/v2.api/loginUser/:api_key", function($api_key) use($app) {
	    if($app->request->post("email") != null &&
	        $app->request->post('password') != null
        ) {
            $dbUtil = DbUtil::Instance();
            $dbUtil->loginForV2($api_key, $app->request->post('email'), $app->request->post('password'));
        } else {
            echo '{"message" : "Request error"}';
        }
	});
	
	$app->post('/v2.api/getSelectionDetail/:api_key', function($api_key) use($app) {
	   if($app->request->post('name') != null) {
	       $dbUtil = DbUtil::Instance();
	       $dbUtil->getSelectionDetailForV2($api_key, $app->request->post('name'));
	   } else {
	       echo '{"message": "Request error"}';
	   }
	});

	$app->notFound(function () use ($app) {
		$error = '
			<!DOCTYPE html>
			<html>
				<head>
					<meta name="og:title" content="Fresher Welcome | UCSTgo"/>
					<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

					<meta name="description" content="Page(' . $app->request->getPath() . ') not found">
					<meta property="og:description" content="Page(' . $app->request->getPath() . ') not found" />
		
					<meta name="og:keywords" content="ucstgo,cutgo,cu taungoo,ucs taungoo,ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ),cutgo voting, fresher welcome"/>
					<meta name="og:type" content="ucstgo,cutgo,cu taungoo,ucs taungoo,ကွန်ပျူတာတက္ကသိုလ်(တောင်ငူ),cutgo voting, fresher welcome"/>

					<meta property="og:image" content="https://ucstaungoo.app/voting/resource/img/ucst.jpg" />
					<meta name="og:image:alt" content="Fresher Welcome | UCSTgo"/>

					<meta name="og:author" content="Kyaw Htut"/>
					<meta name="author" content="Kyaw Htut">
		
					<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		
					<meta name="theme-color" content="#2196f3">

					<title>Page Not Found | UCSTgo</title>
					<link rel="shortcut icon" href="resource/img/logo.png" />
					
					<style>
						body {
							margin: 50px 0 0;
							padding: 0;
							width: 100%;
							font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
							text-align: center;
							color: #aaa;
							font-size: 18px;
						}

						h3 {
							color: #fb0031;
							letter-spacing: -3px;
							font-family: "Lato", sans-serif;
							font-size: 50px;
							font-weight: 200;
							margin-bottom: 0;
						}
					</style>
				</head>
				<body>
					<h3>Page Not Found</h3>
					<h5>The equest page "' . $app->request->getPath() . '" could not be found.</h5>
					<a href=".">Home</a>
				</body>
			</html>
		';
		die($error);
	});

	function isLogin(){
		if(isset($_SESSION['islogin'])){
			$login = $_SESSION['islogin'];
			if($login){
				$dbutil = DbUtil::Instance();
				return $dbutil->isLoginUserAccount($_SESSION['user_name'],$_SESSION['password']);
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	$app->run();